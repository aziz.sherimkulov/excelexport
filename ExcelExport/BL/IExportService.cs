﻿using System.Collections.Generic;

namespace ExcelExport.BL
{
    public interface IExportService
    {
        List<Data.Data> GetAllData();
        Data.Data GetData();
    }
}
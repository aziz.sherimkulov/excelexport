﻿using System.Collections.Generic;
using System.Linq;
using ExcelExport.Data;

namespace ExcelExport.BL
{
    public class ExportService : IExportService
    {
        private readonly ApplicationDbContext _application;

        public ExportService(ApplicationDbContext application)
        {
            _application = application;
        }

        public List<Data.Data> GetAllData()
        {
            var datas = _application.Data.ToList();
            return datas;
        }

        public Data.Data GetData()
        {
            var datas = GetAllData();
            var data = datas.FirstOrDefault(x => x.SocialNumber == "12345543211234");
            return data;
        }
    }
}
﻿using ExcelExport.Seed;
using Microsoft.EntityFrameworkCore;

namespace ExcelExport.Data
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Data> Data { get; set; }   
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Seed();
        }

    }
}
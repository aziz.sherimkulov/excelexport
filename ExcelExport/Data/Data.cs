﻿using System;

namespace ExcelExport.Data
{
    public class Data
    {
        public int Id { get; set; } 
        public string Name { get; set; } 
        public DateTime BirthDate { get; set; } 
        public string Phone { get; set; } 
        public string Address { get; set; } 
        public string SocialNumber { get; set; }     
    }
}
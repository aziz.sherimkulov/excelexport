﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ClosedXML.Excel;
using ExcelExport.BL;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ExcelExport.Models;

namespace ExcelExport.Controllers
{
    public static class SetValueWithColor
    {
        public static void Set(this IXLWorksheet ws, string cell, string value)
        {
            ws.Cell(cell).Value = value;
            ws.Cell(cell).Style.Fill.BackgroundColor = XLColor.Green;
        }
    }
    public class HomeController : Controller
    {
        private readonly IExportService _exportService;
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger, IExportService exportService)
        {
            _logger = logger;
            _exportService = exportService;
        }

        public IActionResult Index()
        {
            var datas = _exportService.GetAllData();
            return View(datas);
        }

        public IActionResult Export()
        {
            var data = _exportService.GetData();
            using (XLWorkbook workbook = new XLWorkbook(XLEventTracking.Disabled))
            {
                var workSheet = workbook.Worksheets.Add("Data");

                workSheet.Cell("A1").Value = "Дата заполнения:";
                workSheet.Cell("A3").Value = "ID:";
                workSheet.Cell("A4").Value = "Фамилия и имя:";
                workSheet.Cell("A5").Value = "Дата рождения:";
                workSheet.Cell("A6").Value = "Номер телефона:";
                workSheet.Cell("A7").Value = "Адресс:";
                workSheet.Cell("A8").Value = "ИНН:";

                workSheet.Set("B1", DateTime.Now.ToString("D"));
                workSheet.Set("B3", data.Id.ToString());
                workSheet.Set("B4", data.Name);
                workSheet.Set("B5", data.BirthDate.ToString("D"));
                workSheet.Set("B6", data.Phone);
                workSheet.Set("B7", data.Address);
                workSheet.Set("B8", data.SocialNumber);

                workSheet.Cell("D4").Value = "ID";
                workSheet.Cell("E4").Value = "Фамилия и имя:";
                workSheet.Cell("F4").Value = "Дата рождения:";
                workSheet.Cell("G4").Value = "Номер телефона:";
                workSheet.Cell("H4").Value = "Адресс:";
                workSheet.Cell("I4").Value = "ИНН:";

                workSheet.Set("D5", data.Id.ToString());
                workSheet.Set("E5", data.Name);
                workSheet.Set("F5", data.BirthDate.ToString("D"));
                workSheet.Set("G5", data.Phone);
                workSheet.Set("H5", data.Address);
                workSheet.Set("I5", data.SocialNumber);

                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    stream.Flush();

                    return new FileContentResult(stream.ToArray(),
                        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                    {
                        FileDownloadName = $"data_{DateTime.UtcNow.ToShortDateString()}.xlsx"
                    };
                }

            }
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

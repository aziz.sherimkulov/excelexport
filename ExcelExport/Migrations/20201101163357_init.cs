﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace ExcelExport.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Data",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(nullable: true),
                    BirthDate = table.Column<DateTime>(nullable: false),
                    Phone = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    SocialNumber = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Data", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Data",
                columns: new[] { "Id", "Address", "BirthDate", "Name", "Phone", "SocialNumber" },
                values: new object[,]
                {
                    { 1, "Баткен", new DateTime(1991, 3, 8, 0, 0, 0, 0, DateTimeKind.Unspecified), "Тестовый клиент1", "123", "12345678901234" },
                    { 2, "Бишкек", new DateTime(1996, 4, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "Тестовый клиент2", "456", "98765432101234" },
                    { 3, "Нарын", new DateTime(1995, 8, 4, 0, 0, 0, 0, DateTimeKind.Unspecified), "Тестовый клиент3", "789", "12345543211234" },
                    { 4, "Комсомольская", new DateTime(1989, 2, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "Тестовый клиент1", "012", "12345543211234" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Data");
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore;

namespace ExcelExport.Seed
{
    public static class DataSeed
    {
        public static void Seed(this ModelBuilder model)
        {
            model.Entity<Data.Data>().HasData(
                new Data.Data
                {
                    Id = 1,
                    Name = "Тестовый клиент1",
                    BirthDate = new DateTime(1991, 3, 8),
                    Phone = "123",
                    Address = "Баткен",
                    SocialNumber = "12345678901234"
                },
                new Data.Data
                {
                    Id = 2,
                    Name = "Тестовый клиент2",
                    BirthDate = new DateTime(1996, 4, 20),
                    Phone = "456",
                    Address = "Бишкек",
                    SocialNumber = "98765432101234"
                },
                new Data.Data
                {
                    Id = 3,
                    Name = "Тестовый клиент3",
                    BirthDate = new DateTime(1995, 8, 4),
                    Phone = "789",
                    Address = "Нарын",
                    SocialNumber = "12345543211234"
                },
                new Data.Data
                {
                    Id = 4,
                    Name = "Тестовый клиент1",
                    BirthDate = new DateTime(1989, 2, 25),
                    Phone = "012",
                    Address = "Комсомольская",
                    SocialNumber = "12345543211234"
                }
            );
        }
    }
}